import '../polyfills';

import { HttpClientModule } from '@angular/common/http';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DemoMaterialModule} from '../material-module';
import {APP_BASE_HREF} from '@angular/common';
import {MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';

import { routing } from './app.routing';
import { UsersComponent } from './users/users.component';
import { CatalogsComponent } from './catalogs/catalogs.component';
import { FlyersComponent } from './flyers/flyers.component';
import { DialogWarningComponent } from './flyers/dialog-warning.component';

@NgModule({
imports: [
  BrowserModule,
  BrowserAnimationsModule,
  FormsModule,
  HttpClientModule,
  DemoMaterialModule,
  MatNativeDateModule,
  ReactiveFormsModule,
  routing
],
entryComponents: [AppComponent, DialogWarningComponent],
declarations: [AppComponent, HomeComponent, LoginComponent, UsersComponent, CatalogsComponent, FlyersComponent, DialogWarningComponent],
bootstrap: [AppComponent],
providers: [
  {provide: APP_BASE_HREF, useValue: ''},
  {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
],
schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
})

export class AppModule {}