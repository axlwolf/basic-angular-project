import { Component, OnInit, Inject, Injectable } from '@angular/core';

@Component({
  selector: 'dialog-warning',
  templateUrl: 'warningDialog.html',
})
@Injectable({
  providedIn: "root"
})
export class DialogWarningComponent {}
