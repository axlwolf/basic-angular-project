import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog} from '@angular/material';
import { DialogWarningComponent } from './dialog-warning.component';
import { AuthenticationService } from '../_services/auth.service';

import {interval, Subscription} from 'rxjs';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/take';

@Component({
  selector: 'app-flyers',
  templateUrl: './flyers.component.html',
  styleUrls: ['./flyers.component.css'],
  providers: [AuthenticationService],
})
export class FlyersComponent implements OnInit {
  subscription: Subscription;

  constructor(public authService$: AuthenticationService,public dialog: MatDialog,private router: Router) {}

  ngOnInit() {
    this.openDialog();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogWarningComponent, { disableClose: true });

    this.subscription = interval(3000).take(4).subscribe(() => {
      dialogRef.close();
      this.logout()
    });

  }

  logout() {
      this.authService$.logout();
      this.router.navigate(['/login']);
  }

  ngOnDestroy() {
   this.subscription.unsubscribe();
 }

}
