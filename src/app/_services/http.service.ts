import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root',
})
export class SimpleHttpService {
  constructor(private http: HttpClient) {}

  public get( url: string, options: object ): Observable<any>  {
    return this.http.get(url, options);
  }

  public post( url: string, body?: any, options?: object ): Observable<any>  {
    if ( !options ) {
      return this.http.post(url, body);
    } else {
        return this.http.post(url, body, options);
      }
  }

  public put( url: string, body: any, options?: object ): Observable<any>  {
    if ( !options ) {
      return this.http.put(url, body);
    } else {
      return this.http.put(url, body, options);
    }
  }

  public delete( url: string, options: object ): Observable<any>  {
    return this.http.delete(url, options);
  }

  private verifyHttpOptions(options: any): object {
    // if we have Headers Verify this is HttpHeaders
    debugger;
    if ( options.headers && options.headers.headers === undefined) {
      let headerArray =  new HttpHeaders();

      options.headers.forEach(function(value, header) {
        headerArray = headerArray.append(header, value);
      });

      options.headers = new HttpHeaders();
      options.headers = headerArray;
    }

    return options;
  }
}
