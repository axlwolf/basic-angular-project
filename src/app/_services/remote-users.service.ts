import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { SimpleHttpService } from './http.service';

@Injectable({
  providedIn: 'root',
})
export class RemoteUsersService {

    private serviceUrl = 'https://jsonplaceholder.typicode.com';  // URL to web api
    constructor(private http: SimpleHttpService) { }

    getUsers() {
      const url = `${this.serviceUrl}/users`;

      return this.http.get(url,  { withCredentials: true });
    }

    getCatalogs() {
      const url = `${this.serviceUrl}/photos`;

      return this.http.get(url,  { withCredentials: true });
    }


}