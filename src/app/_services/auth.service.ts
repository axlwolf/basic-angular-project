import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError, of } from 'rxjs';

import { User } from '../_models/user';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    public users: User[] = [
        { id: 1, username: 'admin', password: 'admin', firstName: 'Axel', lastName: 'Lanuza' }
    ];


    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        const user = this.users.find(x => x.username === username && x.password === password);
        if (!user) return this.error('Username or password is incorrect');
        return this.ok({
            id: user.id,
            username: user.username,
            firstName: user.firstName,
            lastName: user.lastName,
            token: `fake-jwt-token`
        });
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    error(message) {
        return throwError({ status: 400, error: { message } });
    }

    ok(user) {
         if (user && user.token) {
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user);
        }

        const body = user;

        return of(new HttpResponse({ status: 200, body }));
    }
}
