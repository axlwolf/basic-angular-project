import {Component, OnInit, Input, OnDestroy } from '@angular/core';

import { RemoteUsersService } from '../_services/remote-users.service';
import { Subscription }   from 'rxjs';
import { Catalogs } from '../_models/catalogs';

@Component({
  selector: 'app-catalogs',
  templateUrl: './catalogs.component.html',
  styleUrls: ['./catalogs.component.css']
})
export class CatalogsComponent implements OnInit {
  catalog: Catalogs;
  ramdomImg: string;
  imageDescription: string;
  subscription: Subscription;


  constructor(public RemoteUsersService$: RemoteUsersService) {
    this.subscription = RemoteUsersService$.getCatalogs().subscribe(
      catalogs => {
        const randomId = Math.floor((Math.random() * 1000) + 1);
        this.catalog = catalogs.filter(catalog => catalog.id === randomId);
        this.ramdomImg = this.catalog[0].url;
        this.imageDescription = this.catalog[0].title;
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
   this.subscription.unsubscribe();
 }

}
