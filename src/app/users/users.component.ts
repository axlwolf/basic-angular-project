import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {Sort} from '@angular/material';

import { RemoteUsersService } from '../_services/remote-users.service';
import { Subscription }   from 'rxjs';
import { RemoteUsers } from '../_models/remoteUsers';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
users: RemoteUsers[];
sortedData: RemoteUsers[];
subscription: Subscription;

constructor(public RemoteUsersService$: RemoteUsersService) {
  this.subscription = RemoteUsersService$.getUsers().subscribe(
    users => {
      users.forEach(user => {
        delete user.company;
        delete user.address;
      });
      this.users = users;
      this.sortedData = this.users.slice();
  });
}

  ngOnInit() {}

  sortData(sort: Sort) {
    const data = this.users.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'id': return compare(a.id, b.id, isAsc);
        case 'name': return compare(a.name, b.name, isAsc);
        case 'username': return compare(a.username, b.username, isAsc);
        case 'email': return compare(a.username, b.username, isAsc);
        case 'phone': return compare(a.phone, b.phone, isAsc);
        default: return 0;
      }
    });
  }

  ngOnDestroy() {
   this.subscription.unsubscribe();
 }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
