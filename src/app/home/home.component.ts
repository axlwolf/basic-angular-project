import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Router } from '@angular/router';
import {forkJoin} from 'rxjs';
import 'rxjs/add/operator/takeWhile';

import { RemoteUsersService } from '../_services/remote-users.service';
import { AuthenticationService } from '../_services/auth.service';
import { User } from '../_models/user';

@Component({
selector: 'app-home',
templateUrl: 'home.component.html',
styleUrls: ['home.component.scss'],
providers: [RemoteUsersService, AuthenticationService],
})
export class HomeComponent {
  isActive = false;
  remoteUsers = [];
  remoteCatalogs = [];
  currentUser: User;
  isVisible: number = 0;

  menuItems = [
  { title: 'Users', isActive: false},
  { title: 'Catalogs', isActive: false},
  { title: 'Flyers', isActive: false }
];

  constructor(public RemoteUsersService$: RemoteUsersService, public authService$: AuthenticationService, private router: Router) {
    this.loadData();
  }

  ngOnInit(): void {}

  AfterViewInit() {}

  toggleClassName() {
    this.isActive = !this.isActive;
  }

  changeActive(event:Event, index:number) {
    this.isVisible = index + 1;
  }

  loadData() {
     forkJoin([
       /*0*/ this.RemoteUsersService$.getUsers(),
       /*1*/ this.RemoteUsersService$.getCatalogs(),
     ])
       .takeWhile(() => true)
       .subscribe(result => {
         this.remoteUsers = result[0];
         this.remoteCatalogs = result[1];
       });
       this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  logout() {
      this.authService$.logout();
      this.router.navigate(['/login']);
  }

}
